//parameters
button_parameters = [1, 2, 3, 4, 5, 6, 7, 8]
 
var showMenu = false;

function toggleMenu(event){
    console.log(event.target.className.split(' '))
    if(showMenu && !(event.target.className.split(' ').includes('secondary-button'))){
        for(var number in button_parameters){
            console.log('button' + button_parameters[number])
            document.getElementById('button' + button_parameters[number]).className = 'button buttons secondary-button derotating' + button_parameters[number]
        }
        showMenu = false
    } else if (!showMenu && event.target.className.split(' ').includes('main-button')) {
        for(var number in button_parameters){
            console.log('button' + button_parameters[number])
            document.getElementById('button' + button_parameters[number]).className = 'button buttons secondary-button rotating' + button_parameters[number]
        }
        showMenu = true
    }
}

window.addEventListener("click", toggleMenu);
